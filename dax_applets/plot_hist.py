#!/usr/bin/env python3

import numpy as np
import pyqtgraph

import artiq.applets.simple

from dax_applets.base.widget import PlotWidget


class MultiHistogramPlot(PlotWidget):
    """Plot histogram applet for multiple histograms.

    This applet is not compatible with the ARTIQ plot histogram applet.
    """
    DEFAULT_PLOT_NAME = 'Plot {index}'

    def __init__(self, args, req, **kwargs):
        # Call super
        super().__init__(args, req, **kwargs)

        # Set plot names - either a format string or a list of names
        num_names = len(args.plot_names)
        if num_names == 0:
            self._plot_names = self.DEFAULT_PLOT_NAME
        elif num_names == 1:
            self._plot_names = args.plot_names[0]
            if '{index}' not in self._plot_names:
                # Append index
                self._plot_names += ' {index}'
        else:
            self._plot_names = args.plot_names

        # Set labels
        self.plotItem.setLabel('bottom', args.x_label)
        self.plotItem.setLabel('left', args.y_label)

        # Disable mouse tracking (mitigates qt hover bug)
        self.setMouseTracking(False)
        # Enable legend
        self.plotItem.addLegend()

    def update_applet(self, args):
        # Obtain data
        y = np.asarray(self.get_dataset(args.y))
        x = np.asarray(self.get_dataset(args.x, np.arange(-0.5, y.shape[1], dtype=np.float_)))

        # Verify input data
        if not len(y) or not y.shape[1] or y.shape[1] + 1 > len(x):
            return
        if y.ndim != 2:
            raise ValueError('Y data must have two dimensions')

        if len(x) > y.shape[1] + 1:
            # Trim x data
            x = x[:y.shape[1] + 1]

        total_indices = len(y)
        if args.index:
            # Indices are provided, only plot selected histograms
            y = y[args.index]

        # Clear plot
        self.plotItem.clear()

        indices = args.index if args.index else range(total_indices)
        for counts, i in zip(y, indices):
            # Assemble name of the plot
            if isinstance(self._plot_names, str):
                name = self._plot_names.format(index=i)
            else:
                name = self._plot_names[i] if i < len(self._plot_names) else self.DEFAULT_PLOT_NAME.format(index=i)
            # Pick a color and plot histogram
            color = pyqtgraph.intColor(i, hues=total_indices)
            self.plotItem.plot(x=x, y=counts, stepMode=True, fillLevel=0.0, fillBrush=color, name=name)


def main():
    # Create an applet object
    applet = artiq.applets.simple.TitleApplet(MultiHistogramPlot, default_update_delay=0.1)

    # Add custom arguments
    applet.argparser.add_argument("--x-label", default=None, type=str, help="The X label")
    applet.argparser.add_argument("--y-label", default=None, type=str, help="The Y label")
    applet.argparser.add_argument("--index", nargs='+', default=[], type=int,
                                  help="Only plot the histograms at the given indices (plots all by default)")
    applet.argparser.add_argument("--plot-names", nargs='+', default=[], type=str,
                                  help="Prefix of numbered plot names (formatting with `{index}` possible), "
                                       "or a list of names for each plot")

    # Add datasets
    applet.add_dataset("y", "Y data (multiple histograms)")
    applet.add_dataset("x", "X values (bin boundaries)", required=False)
    applet.run()


if __name__ == "__main__":
    main()
