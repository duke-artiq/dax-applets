{ buildPythonPackage
, nix-gitignore
, lib
, numpy
, pyqt5
, pyqtgraph
, artiq
, version ? "0.0.0"
}:

buildPythonPackage {
  pname = "dax-applets";
  inherit version;
  src = nix-gitignore.gitignoreSource [ "*.nix" "/*.lock" ] ./.;

  propagatedBuildInputs = [
    numpy
    pyqt5
    pyqtgraph
    artiq
  ];

  condaDependencies = [
    "python>=3.8"
    "numpy"
    "pyqt>=5,<6"
    "pyqtgraph"
    "artiq"
  ];

  dontWrapQtApps = true;

  meta = with lib; {
    description = "Applets for the ARTIQ dashboard";
    maintainers = [ "Duke University" ];
    homepage = "https://gitlab.com/duke-artiq/dax-applets";
    license = licenses.gpl3Only;
  };
}
