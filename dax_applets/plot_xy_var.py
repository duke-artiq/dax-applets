#!/usr/bin/env python3

import numpy as np
import pyqtgraph

import artiq.applets.simple

from dax_applets.base.widget import PlotWidget


class XYPlotVar(PlotWidget):
    """Matplotlib pyplot like applets for a variable number of plots."""

    PLOT_ARGS_KEY = 'plots'

    def __init__(self, args, req, **kwargs):
        # Call super
        super().__init__(args, req, **kwargs)

        # Process plot varargs
        plots_args = getattr(args, self.PLOT_ARGS_KEY)
        self.plots_xy = [(plots_args[i], plots_args[i+1]) for i in range(0, len(plots_args), 3)]
        self.plots_fmt = [self.map_fmt(f) for f in plots_args[2::3]]

        # Set labels
        self.plotItem.setLabel('bottom', args.x_label)
        self.plotItem.setLabel('left', args.y_label)

        # Disable mouse tracking (mitigates qt hover bug)
        self.setMouseTracking(False)
        # Enable legend
        self.plotItem.addLegend()

    def update_applet(self, args):
        # Obtain trimmed datasets zipped with formatting and labels
        labels = self.get_dataset(args.labels, [])
        plots_xy_fmt_label = [
            (
                (x[:len(y)] if len(x) > len(y) else x, y),   # Trim x data, pass y data
                fmt,  # Pass format
                labels[i] if i < len(labels) else y_key.rsplit(".", maxsplit=1)[-1],  # Construct label
            )
            for x, y, fmt, i, y_key in (
                (
                    np.asarray(self.get_dataset(x_key)), np.asarray(self.get_dataset(y_key)),  # Get datasets
                    fmt,  # Pass format
                    i,  # Pass index
                    y_key,  # Pass y key
                )
                for i, ((x_key, y_key), fmt) in enumerate(zip(self.plots_xy, self.plots_fmt))
            ) if len(y) and len(x) >= len(y)  # Only include valid datasets
        ]
        h_lines = self.get_dataset(args.h_lines, [])
        v_lines = self.get_dataset(args.v_lines, [])

        if not plots_xy_fmt_label:
            # No plots
            return

        # Clear plot
        self.plotItem.clear()

        # Plot horizontal and vertical lines
        for h in h_lines:
            self.plotItem.addLine(y=h)
        for v in v_lines:
            self.plotItem.addLine(x=v)

        for (x, y), (fmt, sort), label in plots_xy_fmt_label:
            if sort:
                # Sort based on x data
                idx = x.argsort()
                x = x[idx]
                y = y[idx]

            # Plot data
            self.plotItem.plot(x=x, y=y, name=label, **fmt)

    @classmethod
    def map_fmt(cls, fmt):
        """Map the format argument to pyqtgraph kwargs and a sort flag.

        The format string is a combination of a color and a symbol.

        The following colors are supported:

        - A single integer for pyqtgraph `intColor` (recommended for simple high-contrast color picking)
        - `"r"`, `"g"`, `"b"`, `"c"`, `"m"`, `"y"`, `"k"`, `"w"`

        The following symbols are currently supported:

        - `"-"` line (for a line plot)
        - `"o"` circle
        - `"s"` square
        - `"t"` triangle
        - `"d"` diamond
        - `"+"` plus
        - `"p"` pentagon
        - `"h"` hexagon
        - `"x"` cross

        :return: (kwargs, sort)
        """
        assert isinstance(fmt, str)

        if len(fmt) != 2:
            raise ValueError("Invalid format string '{}', must have length 2".format(fmt))

        # Extract the color and the style
        color, symbol = fmt

        if color.isdecimal():
            # Convert int colors
            color = pyqtgraph.intColor(color)

        # Convert values to pyqtgraph arguments
        if symbol == "-":
            # Line plot
            return {"pen": color}, True
        else:
            # Scatter plot
            return {"pen": None, "symbol": symbol, "symbolBrush": color}, False


class _TitleAppletVarargs(artiq.applets.simple.TitleApplet):
    def __init__(self, *args, **kwargs):
        # Call super
        super().__init__(*args, **kwargs)

        # Add varargs for a variable amount of plots
        self.argparser.add_argument(XYPlotVar.PLOT_ARGS_KEY, nargs='*', type=str,
                                    help="Plot arguments in triplets: x, y, fmt, ...")

    def args_init(self):
        # Call super
        super().args_init()

        # Check varargs
        plots_args = self.args.plots
        if len(plots_args) % 3:
            raise RuntimeError('Plot arguments must be a sequence of triplets')

        # Add varargs to subscribed datasets
        for i in range(0, len(plots_args), 3):
            self.datasets.add(plots_args[i])  # X-dataset key
            self.datasets.add(plots_args[i+1])  # Y-dataset key


def main():
    # Create applet object
    applet = _TitleAppletVarargs(XYPlotVar, default_update_delay=0.1)

    # Add custom arguments
    applet.argparser.add_argument("--x-label", default=None, type=str, help="The X label")
    applet.argparser.add_argument("--y-label", default=None, type=str, help="The Y label")

    # Add datasets
    applet.add_dataset("labels", "Plot labels", required=False)
    applet.add_dataset("v-lines", "Vertical lines", required=False)
    applet.add_dataset("h-lines", "Horizontal lines", required=False)
    applet.run()


if __name__ == "__main__":
    main()
