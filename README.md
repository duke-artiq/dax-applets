# DAX applets

This repository contains applets for [DAX](https://gitlab.com/duke-artiq/dax)
and the [ARTIQ](https://github.com/m-labs/artiq) dashboard.

> **Note:**
> Due to the significant changes made to applets in ARTIQ 8, this branch only supports ARTIQ 8 and newer.
> For ARTIQ 7 and older, use the `release-7` branch.

## Installation

The DAX installation manual includes instructions to install DAX applets.

- [External users](https://gitlab.com/duke-artiq/dax/-/wikis/DAX/Installation)
- [Duke users](https://gitlab.com/duke-artiq/dax/-/wikis/DAX/Installation%20Duke%20users)
