{
  inputs = {
    artiqpkgs.url = git+https://github.com/m-labs/artiq?ref=release-8;
    nixpkgs.follows = "artiqpkgs/nixpkgs";
  };

  outputs = { self, nixpkgs, artiqpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      dax-applets = pkgs.python3Packages.callPackage ./derivation.nix {
        inherit (artiqpkgs.packages.x86_64-linux) artiq;
        version = "${builtins.toString self.sourceInfo.lastModifiedDate or 0}+${self.sourceInfo.shortRev or "unknown"}";
      };
    in
    {
      packages.x86_64-linux = {
        inherit dax-applets;
        default = dax-applets;
      };

      devShells.x86_64-linux = {
        default = pkgs.mkShell {
          packages = [
            (pkgs.python3.withPackages (ps: [
              # Packages required for testing
              ps.autopep8
              ps.mypy
              ps.flake8
            ] ++ dax-applets.propagatedBuildInputs))
          ];
        };
      };

      # enables use of `nix fmt`
      formatter.x86_64-linux = pkgs.nixpkgs-fmt;
    };

  nixConfig = {
    extra-trusted-public-keys = [
      "nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc="
    ];
    extra-substituters = [ "https://nixbld.m-labs.hk" ];
  };
}
