#!/usr/bin/env python3

import numpy as np
import collections.abc

import artiq.applets.simple
import artiq.applets.big_number
from artiq.tools import scale_from_metadata


class NumberWidget(artiq.applets.big_number.NumberWidget):
    """Extended and backwards compatible version of the standard ARTIQ big number applet."""

    def data_changed(self, value, metadata, persist, mods):
        try:
            self.metadata = metadata[self.dataset_name]
            # This applet will degenerate other scalar types to native float on edit
            # Use the dashboard ChangeEditDialog for consistent type casting
            val = value[self.dataset_name]
            if isinstance(val, (collections.abc.Sequence, np.ndarray)):
                # Pick the last element if we are dealing with a sequence
                val = val[-1]
            val = float(val)
            scale = scale_from_metadata(self.metadata)
            val /= scale
        except (KeyError, ValueError, TypeError):
            val = "---"

        unit = self.metadata.get("unit", "")
        self.unit_area.setText(unit)
        self.lcd_widget.display(val)
